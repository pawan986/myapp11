package com.indiaXI.account.IndiaXIAccount.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.indiaXI.account.IndiaXIAccount.Model.ResponseModel;
import com.indiaXI.account.IndiaXIAccount.Model.UserRespnseModel;;




@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) throws Exception {
		
		
		ResponseModel exceptionResponse=	new  ResponseModel(false,ex.getMessage(),null,0);
	
	return new ResponseEntity(exceptionResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<Object> handleUserNotFoundExceptions(Exception ex, WebRequest request) throws Exception {
		
		
		ResponseModel exceptionResponse=	new ResponseModel(true,ex.getMessage(),null,0);
	
	return new ResponseEntity(exceptionResponse,HttpStatus.NOT_FOUND);
		
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		 List<String> errors = new ArrayList<String>();
		    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
		        errors.add(error.getField() + ": " + error.getDefaultMessage());
		    }
		    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
		        errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		    }
		
		    ResponseModel exceptionResponse=	new ResponseModel(false, errors.toString(),null,0);
	
	return new ResponseEntity(exceptionResponse,HttpStatus.BAD_REQUEST);
	}
	
	

}
