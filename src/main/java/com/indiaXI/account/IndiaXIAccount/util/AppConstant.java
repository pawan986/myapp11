
package com.indiaXI.account.IndiaXIAccount.util;

/**
 * @author Pawan Tripathi
 *
 */
public class AppConstant {
	
	public static final String CLIENT_ID = "myap-client";
	public static final String CLIENT_SECRET = "myapp13";
	public static final String GRANT_TYPE_PASSWORD = "password";
	public static final String AUTHORIZATION_CODE = "authorization_code";
	public static final String REFRESH_TOKEN = "refresh_token";
	public static final String IMPLICIT = "implicit";
	public static final String SCOPE_READ = "read";
	public static final String SCOPE_WRITE = "write";
	public static final String TRUST = "trust";
	public static final String USER_NOT_FOUND = "User Not Found";
	public static final String USER_ALREADY_EXIST = "User Already Exist Found";
	public static final String ENTER_CORRECT_EMAIL = "Please enter Correct email";
	public static final String USER_REGISTRATION_SUCCESSFULLY = "User registration successful";
	public static final String USER_LOGIN_SUCCESSFULLY = "Login successful";
	public static final String User_NAME_EMPTY = "Please Enter Name";
	public static final String User_Mobile_EMPTY = "Please Enter Mobile";
	public static final String User_Password_EMPTY = "Please Enter Password";
	public static final String User_Address_EMPTY = "Please Enter Address";
	public static final String User_ID_EMPTY = "Please Enter UserId";
	public static final String Order_ID_EMPTY = "Please Enter OrderId";
	public static final String Order_Status_EMPTY = "Please Enter OrderStatus";
	public static final String Product_Responce = "Product Added Succesfully";
	public static final String Product_Cart_Responce = "Product Added to cart Succesfully";
	public static final String Product_Name_Emptiy = "Please Enter Produtname";
	public static final String Product_Price_Emptiy = "Please Enter ProductPrice";
	public static final String Product_ID_Emptiy = "Please Enter Productid";
	public static final String Product_Quantiy_Emptiy = "Please Enter Product Quantity";
	public static final String Cart_Name_Emptiy = "Please Enter Produtname";
	public static final String Cart_Price_Emptiy = "Please Enter ProductPrice";
	public static final String Cart_Quantiy_Emptiy = "Please Enter Product Quantity";
	public static final String Addres_Updated_Succesfully = "Address Updated Succesfully";
	public static final String Quantity_Updated_Succesfully = "Quantity Updated Successfully";
	
	public static final String ORDER_SUCCES_STRING = "Order placed succesfully";
	public static final String ORDER_Cancled_STRING = "Order Canceled  Successfully";
	
	}
