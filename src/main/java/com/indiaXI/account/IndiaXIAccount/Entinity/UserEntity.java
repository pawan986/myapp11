package com.indiaXI.account.IndiaXIAccount.Entinity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class UserEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	public Long id;
	

	public UserEntity(Long id, String name, String address, String email,String password,String mobile,
			 String refcode) {
		super();
		this.id = id;
		this.username = name;
		this.address = address;
		this.email = email;
		this.password = password;
		this.mobile=mobile;
		this.refcode=refcode;
	}

	
	public UserEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@Column
	private String username;
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}
	@Column
	private String mobile;
	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	@Column
	private String address;
	@Column
	private String email;
	@Column
	private String password;
	
	@Column
	private String refcode;

	public String getRefcode() {
		return refcode;
	}


	public void setRefcode(String refcode) {
		this.refcode = refcode;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
