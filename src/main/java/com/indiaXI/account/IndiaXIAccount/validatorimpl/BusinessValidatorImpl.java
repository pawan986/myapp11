package com.indiaXI.account.IndiaXIAccount.validatorimpl;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;
import com.indiaXI.account.IndiaXIAccount.Model.UserRespnseModel;
import  com.indiaXI.account.IndiaXIAccount.Model.UserLoginRequest;
import  com.indiaXI.account.IndiaXIAccount.util.AppConstant;
import com.indiaXI.account.IndiaXIAccount.util.NoRespnseModel;
import com.indiaXI.account.IndiaXIAccount.util.StringUtils;
import com.indiaXI.account.IndiaXIAccount.validator.BusinessValidator;

public class BusinessValidatorImpl implements BusinessValidator{

	@Override
	public ResponseEntity<Object> validUserDetails(UserEntity userModel) {
		if (!StringUtils.isEmailPattern(userModel.getEmail())) {
			return new ResponseEntity<Object>(new UserRespnseModel(true, null,AppConstant.ENTER_CORRECT_EMAIL),
					HttpStatus.OK);
		}  else if (StringUtils.isNullOrEmpty(userModel.getUsername())) {
			return new ResponseEntity<Object>(new NoRespnseModel(true,null, AppConstant.User_NAME_EMPTY), 
					HttpStatus.OK);
		}  else if (StringUtils.isNullOrEmpty(userModel.getMobile())) {
			return new ResponseEntity<Object>(new NoRespnseModel(true,null, AppConstant.User_Mobile_EMPTY), 
					HttpStatus.OK);
		}  else if (StringUtils.isNullOrEmpty(userModel.getAddress())) {
			return new ResponseEntity<Object>(new NoRespnseModel(true, null,AppConstant.User_Address_EMPTY), 
					HttpStatus.OK);
		}else {
			return null;
		}
	}

	
	
	@Override
	public ResponseEntity<Object> validUserId(String useid) {
		 if (StringUtils.isNullOrEmpty(useid)) {
			return new ResponseEntity<Object>(new UserRespnseModel(true, null,AppConstant.User_ID_EMPTY), 
					HttpStatus.OK);
		}else {
			
		}	return null;
		
	}



	@Override
	public ResponseEntity<Object> validUserLogin(UserLoginRequest user) {
		 if (StringUtils.isNullOrEmpty(user.getMobile())) {
				return new ResponseEntity<Object>(new UserRespnseModel(true, null,AppConstant.User_Mobile_EMPTY), 
						HttpStatus.OK);
			}else {
				
			}	return null;
	}




	




}
