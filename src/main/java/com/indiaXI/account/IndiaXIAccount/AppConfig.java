package com.indiaXI.account.IndiaXIAccount;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.indiaXI.account.IndiaXIAccount.validator.BusinessValidator;
import com.indiaXI.account.IndiaXIAccount.validatorimpl.BusinessValidatorImpl;



/**Create AppConfig class
 * Create Bean
 * @author Pawan Kumar
 *
 */
@Configuration
public class AppConfig {
	@Bean
	BusinessValidator findIValidator() {
		return new BusinessValidatorImpl();
	}
}
