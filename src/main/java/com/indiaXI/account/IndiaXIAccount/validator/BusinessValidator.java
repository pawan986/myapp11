package com.indiaXI.account.IndiaXIAccount.validator;

import org.springframework.http.ResponseEntity;

import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;
import com.indiaXI.account.IndiaXIAccount.Model.UserLoginRequest;



public interface BusinessValidator {
	public ResponseEntity<Object> validUserDetails(UserEntity userModel);
	public ResponseEntity<Object> validUserLogin(UserLoginRequest user);
	
	
	public ResponseEntity<Object> validUserId(String userid);
	
}
