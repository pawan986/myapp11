package com.indiaXI.account.IndiaXIAccount.Impl;
import java.util.Arrays;
import java.util.List;
import org.hibernate.annotations.common.util.impl.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;
import com.indiaXI.account.IndiaXIAccount.Model.UserLoginRequest;
import com.indiaXI.account.IndiaXIAccount.repository.UserRepository;
import com.indiaXI.account.IndiaXIAccount.service.UserService;

/**
 * User related implementation
 * 
 */


@Service(value = "userService")
public class UserImpl implements UserService ,UserDetailsService {
	@Autowired
	private UserRepository userReposiotry;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

	/*
	 * @Autowired private BCryptPasswordEncoder passwordEncoder;
	 */
	/**
	 * save user information in user table
	 */

	@Override
	public UserEntity addUser(UserEntity user) {
		UserEntity userEntity = new UserEntity();

		userEntity.setUsername(user.getUsername());
		userEntity.setEmail(user.getEmail());
		userEntity.setAddress(user.getUsername());
		userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
		userEntity.setMobile(user.getMobile());
		UserEntity userdata = userReposiotry.save(userEntity);

		return userdata;

	}



	@Override
	public UserEntity finduser(String  mobile) {
		// TODO Auto-generated method stub
		
		 UserEntity userdata = userReposiotry.findByMobile(mobile); 
		
		
			 return userdata;
		
		
	}

	@Override
	public boolean authentication(UserEntity userLoginRequest) {
		boolean login = false;
		 UserEntity userdata = userReposiotry.findByMobile(userLoginRequest.getMobile()); 
			
		if (userdata != null) {
			
			if (passwordEncoder.matches(userLoginRequest.getPassword(), userdata.getPassword()))  {
				login = true;
			} else {
				login = false;
			}
			
		} else {
			login = false;
		}
		return login;
	}


	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		 UserEntity userdata = userReposiotry.findByMobile(username);
			
		if(userdata == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(userdata.getUsername(), userdata.getPassword(), getAuthority());

	}
	

}
