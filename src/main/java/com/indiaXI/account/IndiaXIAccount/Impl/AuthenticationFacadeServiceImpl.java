package com.indiaXI.account.IndiaXIAccount.Impl;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;



@Component
public class AuthenticationFacadeServiceImpl implements com.indiaXI.account.IndiaXIAccount.service.AuthenticationFacadeService {

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
