package com.indiaXI.account.IndiaXIAccount.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;
import com.indiaXI.account.IndiaXIAccount.Model.UserLoginRequest;

/**
 * Method Use to register user
 * 
 * @Param UserEntity
 * @Table UserEntity is use to map user data
 * @Action Register user
 * @Returntype UserEntity
 */

@Service
public interface UserService {
	public UserEntity addUser(UserEntity user);

	public UserEntity finduser(String  mobile);

	boolean authentication(UserEntity userLoginRequest);
}
