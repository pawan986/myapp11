package com.indiaXI.account.IndiaXIAccount.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;
import com.indiaXI.account.IndiaXIAccount.Impl.UserImpl;
import com.indiaXI.account.IndiaXIAccount.Model.UserLoginRequest;
import com.indiaXI.account.IndiaXIAccount.util.AppConstant;
import com.indiaXI.account.IndiaXIAccount.util.NoRespnseModel;
import com.indiaXI.account.IndiaXIAccount.validator.BusinessValidator;

/**
 * Action related User
 * 
 * @author Pawan Tripathi
 *
 */
@RestController
public class UserController {
	@Autowired
	private UserImpl iUser;

	@Autowired(required = true)
	BusinessValidator businessValidator;

	/**
	 * Post mathod for User Registration
	 * 
	 * @param User Request
	 * @return ResponseEntity<Object>
	 * @Excption User Not Found
	 */
	@PostMapping("/registeruser")
	public ResponseEntity<Object> userRegistration(@RequestBody UserEntity request) {
		
		
		
		ResponseEntity<Object> responseEntity = businessValidator.validUserDetails(request);
		if (responseEntity == null) {
			try {

				UserEntity userexist = null;
				try {
					userexist = (UserEntity) iUser.finduser(request.getMobile());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(userexist!=null) {
					return new ResponseEntity<Object>(new NoRespnseModel(false, null, AppConstant.USER_ALREADY_EXIST),
							HttpStatus.INTERNAL_SERVER_ERROR);
				}else {
					UserEntity userData = (UserEntity) iUser.addUser(request);
					return new ResponseEntity<Object>(
							(new NoRespnseModel(true, userData, AppConstant.USER_REGISTRATION_SUCCESSFULLY)),
							HttpStatus.CREATED);
				}
				

			} catch (Exception e) {
				return new ResponseEntity<Object>(new NoRespnseModel(false, null, e.getMessage()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;
		}
	}

	@PostMapping("/login")
	public ResponseEntity<Object> userLogin(@RequestBody UserLoginRequest request) {
	
		ResponseEntity<Object> responseEntity = businessValidator.validUserLogin(request);
		if (responseEntity == null) {
			try {

				UserEntity userexist = null;
				try {
					userexist = (UserEntity) iUser.finduser(request.getMobile());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(userexist!=null) {
					return new ResponseEntity<Object>(new NoRespnseModel(true, null, AppConstant.USER_LOGIN_SUCCESSFULLY),
							HttpStatus.OK);
				}else {
					return new ResponseEntity<Object>(new NoRespnseModel(false, null, AppConstant.USER_NOT_FOUND),
							HttpStatus.NOT_FOUND);
				}
				

			} catch (Exception e) {
				return new ResponseEntity<Object>(new NoRespnseModel(false, null, e.getMessage()),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return responseEntity;
		}
	}
}
