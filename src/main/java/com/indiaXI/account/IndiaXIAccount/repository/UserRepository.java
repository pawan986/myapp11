package com.indiaXI.account.IndiaXIAccount.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiaXI.account.IndiaXIAccount.Entinity.UserEntity;






/**
 * @author Pawan Tripathi
 *
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
	

UserEntity findByMobile(String mobile);


}
